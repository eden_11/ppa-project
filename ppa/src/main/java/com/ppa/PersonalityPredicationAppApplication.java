package com.ppa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class

PersonalityPredicationAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalityPredicationAppApplication.class, args);
	}

}
